
package figurasgeometricas;



public abstract class Figura {
    private String color;
    private String nombre;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public abstract int area(); //metodo abstracto, no se usa metodo directo, son metodos que tienen que estar definidos
    
    public abstract int perimetro();  //metodo abstracto, no se usa metodo directo, son metodos que tienen que estar definidos
    
    public abstract String getType();
}
