/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author sofi
 */
public class Cuadro extends Figura{ //clase cuadro hereda de figura abstracta
    private int lado;
    
    public Cuadro(int lado){
        this.lado = lado;
    }

    public int getLado() {
        return lado;
    }

    public void setLado(int lado) {
        this.lado = lado;
    }
    
    @Override
    public int area() { //son metodos de clase figura que son necesarios tener
        return this.lado * this.lado;
    }

    @Override
    public int perimetro() {
        return this.lado * 4;
    }

    @Override
    public String getType() {
        return "Cuadro";
    }
    
    
}
