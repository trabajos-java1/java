/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author sofi
 */
public class Triangulo extends Figura{
    private int base;
    private int altura;

    
    public Triangulo(int base, int altura){
      this.base = base;
      this.altura = altura;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
    
    @Override
    public int area() {
        return (this.base * 3);
    }

    @Override
    public int perimetro() {
        return (this.base * this.altura)/2;
    }

    @Override
    public String getType() {
        return "Triangulo";
    }
    
}
