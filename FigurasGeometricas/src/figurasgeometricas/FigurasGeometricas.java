/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figurasgeometricas;

/**
 *
 * @author sofi
 */
public class FigurasGeometricas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {                   
        
        Figura[] misFiguras = new Figura[5]; //Object
        misFiguras[0] = new Triangulo(10,5);
        misFiguras[1] = new Cuadro(6);
        misFiguras[2] = new Triangulo(5,3);
        misFiguras[3] = new Triangulo(6,3);
        misFiguras[4] = new Cuadro(7);
        
        for(int i = 0; i < misFiguras.length;i++){
            //Figura figura = (Figura)misFiguras[i];
            System.out.println("El area del " + misFiguras[i].getType() + " es de: "+misFiguras[i].area()); //figura.area()
        }
    }
    
}
